const {createGraph, KahnAlgorithm} = require('../willa_challenge')
const expect = require('chai').expect;

describe('#KahnAlgorithm()', () => {

  context('acyclic graph', () => {
    it('should return nodes in order of creation', () => {
      const vertices = ["A", "B", "C", "D", "E", "F"]
      const edges = [["A", "B"],["A", "C"],["B", "D"],["B", "E"],["C", "D"],["C", "F"],["D", "E"],["F", "E"]]
      const graph = createGraph(vertices, edges)
      expect(KahnAlgorithm(graph)).to
      .deep.equal({'A':0, 'B':1, 'C':2, 'D':3, 'F':4, 'E':5})
    })
  })

  context('cyclic graph', () => {
    it('should throw error', () => {
      const vertices = ['A', 'B', 'C']
      const edges = [['A','B'], ['B','C'], ['C','A']]
      const graph = createGraph(vertices, edges)
      expect(() => {KahnAlgorithm(graph)}).to
      .throw('The graph is cyclic, therefore this graph cannot be topoligically sorted')
    })
  })
})
