import database from './database.json';
module.exports = { createGraph, KahnAlgorithm };

class Graph {

    adjacencyList:{[key:string]:string[]}

    constructor() {
        this.adjacencyList = {};
    }
    addVertex(vertex:string) {
        if (!this.adjacencyList[vertex]) {
            this.adjacencyList[vertex] = [];
        }
    }
    addEdge(vertex1:string, vertex2:string) {
        this.adjacencyList[vertex1].push(vertex2);
    }
}

/**
 * Creates a graph
 * @param vertices vertices are strings, ex:["A", "B", "C", "D", "E", "F"]
 * @param edges edges are string tuples, ex:[["A","B"], ["A","C"]]
 * @returns graph containing vertices and edges
 */
function createGraph(vertices:string[], edges:[string, string][]) {
    const g = new Graph();
    vertices.forEach((vertex:string) => {
        g.addVertex(vertex)
    });
    edges.forEach((edge:[string, string]) => {
        g.addEdge(edge[0], edge[1])
    })
    return g;
}

/**
 * Get vertices from the database model
 * @param database database.json data
 * @returns vertices array of vertices, each vertex is a string, ex:["A", "B", "C", "D", "E", "F"]
 */
function getVertices(database:any) {
    let vertices:string[] = []
    database.forEach((table:any) => {
        vertices.push(table.name)
    })
    return vertices
}

/**
 * Get edges from the database model
 * @param database database.json data
 * @returns edges array of tuples, each edge is an array of 2 strings, ex:[["A","B"], ["A","C"]]
 * edge from A to B if job A must be completed before job B can be started
 */
function getEdges(database:any) {
    let edges:[string,string][] = []
    database.forEach((table:any) => {
        table.columns.forEach((column:any) => {
            if(column.foreign_key != null) {
                edges.push([column.foreign_key.split(".")[0], table.name])
            }
        })
    })
    return edges
}


/**
 * Runs Kahn algorithm on graph
 * @param graph graph representation of database model
 * @returns result ordered list of vertices, ex:{A: 0,B: 1,C: 2,D: 3,F: 4,E: 5 }
 * first element is first to be created
 * @throws will throw an exception in case of cycle in the graph
 */
function KahnAlgorithm(graph:Graph) {
    // Calcuate the incoming degree of each vertex
    const vertices = Object.keys(graph.adjacencyList)
    const inDegree:{[key:string]:number} = {};
    for (const v of vertices) {
        for (let neighbor of graph.adjacencyList[v]) {
            inDegree[neighbor] = inDegree[neighbor] + 1 || 1;
        }
    }

    // Create a queue which stores the vertex without dependencies
    const queue = vertices.filter((vertice:string) => !inDegree[vertice]);
    const topNums:{[key:string]:number} = {};
    let index = 0;
    while (queue.length) {
        const vertice = queue.shift();
        if(vertice != undefined) {
            topNums[vertice] = index++;
            // adjust the incoming degree of its neighbors
            for (const neighbor of graph.adjacencyList[vertice]) {
                inDegree[neighbor]--;
                if (inDegree[neighbor] === 0) {
                    queue.push(neighbor);
                }
            }
        }
    }

    if (index !== vertices.length) {
        console.log("Cycle detected");
        throw "The graph is cyclic, therefore this graph cannot be topoligically sorted";
    }

    return topNums
}

const tables = getVertices(database)
const edges = getEdges(database)
const graph = createGraph(tables, edges)
const result = KahnAlgorithm(graph)
Object.keys(result).forEach((vertex:string) => {console.log(vertex)})

