# Willa Database Builder

Willa Database Builder is a script written with typescript. 

It determines in which order to create database tables.

This is useful when tables are inter-dependant which means some tables need to be created before others.

## Building

In order to build Willa Database Builder, ensure that you have Git and Node.js installed.

Clone a copy of the repo:

```bash
git clone https://gitlab.com/MohamedRed/willa.git
```

Install dev dependencies:

```bash
npm install
```

## Usage

The script uses the database.json file as input. 

```bash
npm start
```

## Test

```bash
npm test
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
